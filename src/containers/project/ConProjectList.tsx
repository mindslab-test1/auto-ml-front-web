import React from 'react';
import {ComPage} from '../../components/commons';
import ComTableSection from '../../components/commons/ComTableSection';

interface Props{}

export interface ApiListResponse{
    id: number;
    modDt: string;
    projectDescription: string;
    projectName: string;
    projectType: string;
    modelName: string;
    regDt: string;
    modelCnt: number;
    status: string;
}

const ConProjectList: React.FC<Props> = () => {

    return (
        <ComPage title={'내가 만든 학습 프로젝트'} subTitle={''} isVisibleSpan={false} engineName={''}>
            <ComTableSection />
        </ComPage>
    );
};

export default React.memo(ConProjectList);
