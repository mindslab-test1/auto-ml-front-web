import React, {useContext, useEffect, useState} from 'react';
import {loginCallback} from 'service/AuthService';
import {IContainerBaseProps} from 'constants/BaseInterface';
import RootStore from '../../store/RootStore';

interface Props extends IContainerBaseProps {}

const ConLoginComplete: React.FC<Props> = (navigation) => {
  const {layoutStore, userStore} = useContext(RootStore);
  const [processing, setProcessing] = useState<boolean>(false);
  const { handleSetProfile } = userStore;
  const param = navigation.location.search;
  console.log(param);

  const handleLoginError = () => {
    setProcessing(false);
    // message.error('로그인에 실패하였습니다.');
  };

  const submit = () => {
    setProcessing(true);
    void loginCallback(navigation, param,() => handleLoginError(), (name, email) => handleSetProfile(name, email));
  };

  useEffect(()=>{
    submit();
  },[])

  return (
    <div>
    </div>
  );
};

export default ConLoginComplete;
