import {observable, action} from 'mobx';
import {pullUserToken} from '../service/AuthService';
import jwt_decode from 'jwt-decode';

export default class UserStore {
  private root: any;

  constructor(root) {
    this.root = root;
    const token = pullUserToken();
    if (token) {
      let decoded = jwt_decode(token);
      this.setName(decoded.name);
      this.setEmail(decoded.email);
    }
  }

  @observable email: string = '';
  @observable name: string = '';

  @action setName = (newName) => {
    this.name = newName;
  }
  @action setEmail = (newEmail) => {
    this.email = newEmail;
  }
  @action getUserName = () => {
    return this.name;
  }
  @action getUserEmail = () => {
    return this.email;
  }

  @action handleSetProfile = (name: string, email: string) => {
    this.setName(name);
    this.setEmail(email);
    if (email.length > 0) {
      this.root.notiStore.info('로그인 되었습니다.');
    }
    else {
      this.root.notiStore.info('로그아웃 되었습니다.');
    }
  }
}
