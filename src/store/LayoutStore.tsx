import { observable, action } from 'mobx';

export default class LayoutStore {
  private root: any;
  constructor(root) {
    this.root = root;
  }

  @observable showHeader: boolean = true;
  @observable showSidebar: boolean = true;
  @observable showContents: boolean = true;

  @action toggleHeader = (toggle: boolean) => {
    this.showHeader = toggle;
  };

  @action toggleSidebar = (toggle: boolean) => {
    this.showSidebar = toggle;
  };
  @action toggleContents = (toggle: boolean) => {
    this.showContents = toggle;
  };
}

