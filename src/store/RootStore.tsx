import LayoutStore from './LayoutStore';
import UserStore from './UserStore';
import SttProjectStore from './SttProjectStore';
import LegacyDataStore from './LegacyDataStore';
import NotificationStore from './NotificationStore';
import TrainingStore from './TrainingStore';
import SttModelTestStore from './SttModelTestStore';

import {createContext} from 'react';


class RootStore {
  layoutStore: LayoutStore;
  userStore: UserStore;
  sttProjectStore: SttProjectStore;
  legacyDataStore: LegacyDataStore;
  notiStore: NotificationStore;
  trainingStore: TrainingStore;
  sttModelTestStore: SttModelTestStore;

  constructor() {
    this.layoutStore = new LayoutStore(this);
    this.userStore = new UserStore(this);
    this.sttProjectStore = new SttProjectStore(this);
    this.legacyDataStore = new LegacyDataStore(this);
    this.notiStore = new NotificationStore(this);
    this.trainingStore = new TrainingStore(this);
    this.sttModelTestStore = new SttModelTestStore(this);
  }
}

export default createContext(new RootStore());
