import { store } from 'react-notifications-component';

export default class NotificationStore {
  private root: any;
  constructor(root) {
    this.root = root;
  }

  private duration: number = 3000;
  private position: any = 'bottom-right';

  error = (message) => {
    console.log('ERROR:' + message);
    this.noti(message, 'danger');
  }

  info = (message) => {
    console.log('INFO:' + message);
    this.noti(message, 'info');
  }

  infoGreen = (message) => {
    console.log('INFO:' + message);
    this.noti(message, 'success');
  }

  infoBlue = (message) => {
    console.log('INFO:' + message);
    this.noti(message, 'default');
  }

  warning = (message) => {
    console.log('WARN:' + message);
    this.noti(message, 'warning');
  }

  private noti = (message: string, type: any) => {
    store.addNotification({
      // title: 'ERROR',
      message: message,
      type: type,
      insert: 'top',
      container: this.position,
      animationIn: ['animated', 'fadeIn'],
      animationOut: ['animated', 'fadeOut'],
      dismiss: {
        duration: this.duration,
        showIcon: true,
        // onScreen: true,
      }
    });
  }
}

// export const info = (message) => {
//   store.addNotification({
//     // title: 'ERROR',
//     message: message,
//     type: 'info',
//     insert: 'top',
//     container: 'bottom-right',
//     animationIn: ['animated', 'fadeIn'],
//     animationOut: ['animated', 'fadeOut'],
//     dismiss: {
//       duration: 3000,
//       showIcon: true,
//       // onScreen: true,
//     }
//   });
// }
