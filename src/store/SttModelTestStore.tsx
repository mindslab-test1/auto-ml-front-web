import {observable, action, computed} from 'mobx';
import {deepCopyObject, guid} from '../utils/CommonUtils';
import {API, callApi, callMockApi, callTestApi} from '../service/ApiService';
import SttProjectStore from './SttProjectStore';

export default class SttModelTestStore {
  private root: any;
  private sttProjectStore: SttProjectStore;
  constructor(root) {
    this.root = root;
    this.sttProjectStore = root.sttProjectStore;
  }

  @observable currentModel;
  @action setCurrentModel = (modelName, id) => {
    // if (this.testModelList !== undefined && this.testModelList.length > 0) {
    //   let model = this.testModelList.find(obj => (obj.mdlId === id));
    //   if (model) {
    //     let modelFile = model.find(obj => (obj.mdlFileName === modelName && obj.mdlId === id));
    //     this.currentModel = modelFile;
    //   }
    // }

    //
    //   for (const model of this.testModelList) {
    //     for(const modelFile of model.modelFileInfoVoList) {
    //
    //     }
    let model = this.testModelList.find(obj => (obj.mdlFileName === modelName && obj.mdlId === id));
    this.currentModel = model;
  }

  @observable testModelList: any = undefined;
  @action setTestModelList = (testModelList) => {this.testModelList = testModelList;}
  // @action getTestModelList = () => {return this.testModelList;}

  @observable modelListDisplay: any = undefined;

  @computed get getTestModelList() {
    console.log('getTestModelList');
    if (this.testModelList === undefined) {
      return [];
      // void this.fetchModelList(this.root.sttProjectStore.id);
    }

    return this.modelListDisplay;
  }

  @observable stepNum : number = 1;
  @action getStepNum(){ return this.stepNum; }
  @action setStepNum(stepNum){ this.stepNum = stepNum; }

  @observable queueIndex : number = 0;
  @action getQueueIndex(){ return this.queueIndex; }
  @action setQueueIndex(queueIndex){ this.queueIndex = queueIndex;}

  @action
  fetchModelList = async (id: number) => {
    const _api = deepCopyObject(API.DMNG_MODELFILE_LIST);
    _api.url = _api.url.replace('_ID_', id);
    const res = await callApi(_api);
    console.log('get fetchTestModelList');
    console.log(res.data);

    if(typeof this.currentModel === 'undefined') {
      this.currentModel = [];
    }
    const modelList = new Map();
    if (res.data !== undefined && res.data.length > 0) {
      this.testModelList = [];
      for (const model of res.data) {
        for(const modelFile of model.modelFileInfoVoList) {

          this.testModelList.push(modelFile);

          const mdlId = modelFile['mdlId'];
          const mdlFileName = modelFile['mdlFileName'];
          if (modelList.get(mdlId) === undefined) {
            modelList.set(mdlId, []);
          }
          modelList.get(mdlId).push(mdlFileName);
        }
      }
    }

    const result = [];

    modelList.forEach((value, key) => {
      const item = {'key': key, 'value': value};
      // @ts-ignore
      result.push(item);
    })

    this.modelListDisplay = result;
  }

  @observable modelTestSnapshotId : number = 1;
  @action getModelTestSnapshotId(){ return this.modelTestSnapshotId; }
  @action setModelTestSnapshotId(modelTestSnapshotId){ this.modelTestSnapshotId = modelTestSnapshotId; }

  @observable modelTestResult : string = '';
  @action getModelTestResult(){ return this.modelTestResult; }
  @action setModelTestResult(modelTestResult){ this.modelTestResult = modelTestResult; }

  // 모델 테스트 생성 요청
  @action
  fetchSttTest = async (src, mdId, mdlFileName, mdlFilePath) => {

    const formData = new FormData();
    formData.append('modelId', mdId);
    formData.append('modelFileName', mdlFileName);
    formData.append('modelFilePath', mdlFilePath);
    formData.append('dataFile', src, guid() + '.wav');

    const _api = deepCopyObject(API.MODEL_TEST_BEGIN);

    await callTestApi(_api, formData).then((res)=>{
      if(res.status === 200){
        this.setModelTestSnapshotId(res.data.modelTestSnapshotId);
        if(res.data.status === 'Waiting') {
          console.log('test : Waiting');
          console.log(res.data.queueIndex);
          this.root.sttModelTestStore.setQueueIndex(res.data.queueIndex);
          this.root.sttModelTestStore.setStepNum(3);
        } else if(res.data.status === 'Running') {
          console.log('Running');
          this.root.sttModelTestStore.setStepNum(4);
        } else if(res.data.status === 'Completed') {
          console.log('Completed');
          this.root.sttModelTestStore.setStepNum(5);
          this.setModelTestResult(res.data.result);
        } else if(res.data.status === 'Error') {
          console.log('Error');
          this.root.sttModelTestStore.setStepNum(6);
        }
        this.root.notiStore.infoGreen('테스트 시작');
        console.dir(res.data);
      }else{
        this.root.notiStore.error('테스트 시작 실패');
        this.root.sttModelTestStore.setStepNum(6);
        console.log('%c HTTP status : ' + res.status + ' :  test begin fail\n', res.data);
      }
    }).catch((res)=>{
      this.root.notiStore.error('테스트 시작 실패');
      this.root.sttModelTestStore.setStepNum(6);
      console.log('%c HTTP status : ' + res.status + ' :  test begin fail\n', res.data);
    })
  }

  // 상태 조회
  @action
  fetchSttTestStatus = async () => {
    const id = this.getModelTestSnapshotId();
    const params = {modelTestSnapshotId: id}
    const _api = deepCopyObject(API.MODEL_TEST_STATUS);


      await callApi(_api, params).then((res)=>{
        console.log(res.data.queurIndex);
        if(res.status === 200){
          console.log(res.data);
          if(res.data.status === 'Waiting') {
            console.log('status : Waiting');
            console.log(res.data.queueIndex);
            this.root.sttModelTestStore.setQueueIndex(res.data.queueIndex);
            this.root.sttModelTestStore.setStepNum(3);
          } else if(res.data.status === 'Running') {
            console.log('Running');
            this.root.sttModelTestStore.setStepNum(4);
          } else if(res.data.status === 'Completed') {
            console.log('Completed');
            this.root.sttModelTestStore.setStepNum(5);
            this.setModelTestResult(res.data.result);
            // clearInterval(interval);
          } else if(res.data.status === 'Error') {
            console.log('Error');
            this.root.sttModelTestStore.setStepNum(6);
            // clearInterval(interval);
          }
        }else{
          this.root.notiStore.error('테스트 status 호출 실패');
          this.root.sttModelTestStore.setStepNum(6);
          // clearInterval(interval);
          console.log('%c HTTP status : ' + res.status + ' :  test status fail\n', res.data);
        }
      }).catch((res)=>{
        this.root.notiStore.error('테스트 status 호출 실패');
        this.root.ttModelTestStore.setStepNum(6);
        // clearInterval(interval);
        console.log('%c HTTP status : ' + res.status + ' :  test status fail\n', res.data);
      })

  };

  // 실행 중인 모델 테스트 취소
  @action
  fetchSttTestCancel = async (id: number) => {
    console.log('get fetchSttTestCancel' + id);

    const param = {modelTestSnapshotId: id,}
    const _api = deepCopyObject(API.MODEL_TEST_CANCEL);
    const res = await callApi(_api, param);
    console.log(res.data);

    this.root.sttModelTestStore.setStepNum(1);
  }
}



