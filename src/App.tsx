import { hot } from 'react-hot-loader/root';
import React, { Component } from 'react';
import { BrowserRouter, Route, Switch, withRouter } from 'react-router-dom';

import { Header, Sidebar, Footer } from 'components/layout';
import { ROUTES } from 'router/RouteLinks';
import {PageNoMatch} from './pages/error';

class App extends Component {
  render() {
    console.log('process.env.NODE_ENV: ' + process.env.NODE_ENV);
    console.log('process.env.REACT_APP_ENV: ' + process.env.REACT_APP_ENV);
    return (
      <BrowserRouter>
        <Header />
        <div id='container' className='view'>
          <Sidebar />
          <div className='contents'>
            <Switch>
              {ROUTES.map((route, index) => (
                <Route key={index} path={route.path.toString()} exact={route.exact} component={withRouter(route.component)}/>
              ))}
              <Route component={PageNoMatch}/>
            </Switch>
          </div>
          {/*<Footer />*/}
        </div>
      </BrowserRouter>
    );
  }
}

export default hot(App);
