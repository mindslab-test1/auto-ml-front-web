export const formatPrice = (value: number) => {
  const amount = formatNumber(value);
  return amount + '원';
};

export const formatNumber = (value: number) => {
  if (value === 0) return 0;

  let amount = value.toString();
  amount = amount.replace(/\D/g, '');
  amount = amount.replace(/\./g, '');
  amount = amount.replace(/-/g, '');

  let numAmount = Number(amount);
  amount = numAmount.toFixed(0).replace(/./g, function (c, i, a) {
    return i > 0 && c !== ',' && (a.length - i) % 3 === 0 ? ',' + c : c;
  });

  return amount;
};

export const scrollTop = () => {
  window.scrollTo(0, 0);
};

export const deepCopyObject = (obj: object) => {
  return JSON.parse(JSON.stringify(obj));
};

export function isObjectEmpty(obj: object) {
  for (let key in obj) {
    if (obj.hasOwnProperty(key)) {
      return false;
    }
  }
  return true;
}

export const defaultDisplay = (code: string) => {
  return code;
}

export const smplRateDisplay = (code: string) => {
  if (code === 'SR001') {
    return '8K';
  }
  else if (code === 'SR002') {
    return '16K';
  }
  else {
    return 'unknown(' + code + ')';
  }
}

export const langCdDisplay = (code: string) => {
  if(code === 'LN_KO') {
    return '한국어';
  }
  else if(code === 'LN_EN') {
    return 'English';
  }
  else {
    return 'unknown(' + code + ')';
  }
}

export function captureUserMedia(callback) {
  const params = { audio: true, video: false };
  navigator.getUserMedia(params, callback, (error) => {
    alert(JSON.stringify(error));
  });
}

export function guid() {
  function s4() {
    return ((1 + Math.random()) * 0x10000 | 0).toString(16).substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}
