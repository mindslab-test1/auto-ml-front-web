import React, {useEffect, useState} from 'react';
import axios from 'axios';
import TextCounter from '../commons/ComTextCounter';
import {API, callApi} from '../../service/ApiService';
import {ERouteUrl} from '../../router/RouteLinks';



interface Props {
    close:  () => void;
    engineUrl: string;
    engineName: string;
    navigation: any;
}


const PopCreateProject: React.FC<Props> = React.memo(({close,engineName, navigation}) => {

    const [prjName, setPrjName] = useState('');
    const [prjDesc, setPrjDesc] = useState('');
    const [btnDisable, setBtnDisable] = useState(true);

    const callApiCreate = async () => {
        try{
            let param = {projectName : prjName, projectDescription : prjDesc, projectType : engineName};
            const res = await callApi(API.PROJECT_STT_REG, param);
            console.dir('Result : ', res.data);
            const route = ERouteUrl.PROJECT_DETAIL.replace(':id', res.data.id);
            navigation.history.push(route);

            // window.location.href = '/prj/' + res.data.id;
        }catch (err){
            console.error(err);
        }
    }

    const onProjectNameChange = (e) => {
        setPrjName(e.target.value);

        (e.target.value.length === 0 || prjDesc.length === 0) ?
          setBtnDisable(true) : setBtnDisable(false);
    }

    const onProjectDescChange = (e) => {
        setPrjDesc(e.target.value);

        (e.target.value.length === 0 || prjName.length === 0) ?
          setBtnDisable(true) : setBtnDisable(false);
    }

    useEffect(()=>{
    },[])

    return (
        <div className='lyrWrap' >
            <div className='lyr_bg' onClick={close}/>
            <div className='lyrBox'>
                <div className='lyr_top'>
                    <h3>새 프로젝트</h3>
                    <button className='btn_lyr_close' onClick={close}>닫기</button>
                </div>
                <div className='lyr_mid'>
                    <div className='dlBox'>
                        <dl className='dl_tbl'>
                            <dt>학습 모델 엔진</dt>
                            <dd><span className='en_type ft_clr_stt'>{engineName}</span></dd>
                        </dl>
                        <dl className='dl_tbl'>
                            <dt>프로젝트 명
                                <span className='byteBox'>
                                   <TextCounter length={prjName.length} maxLength={20}/>
                                </span>
                            </dt>
                            <dd>
                                <input type='text' id='proj_name' className='ipt_txt w100p txtByte' maxLength={20}
                                       placeholder='프로젝트 명은 최대 20자 입력이 가능합니다.' autoComplete='off' onChange={onProjectNameChange} />
                            </dd>
                        </dl>
                        <dl className='dl_tbl'>
                            <dt>설명
                                <span className='byteBox'>
                                     <TextCounter length={prjDesc.length} maxLength={50} />
                                </span>
                            </dt>
                            <dd>
                                <textarea id='proj_desc' className='textArea w100p txtByte' rows={3} maxLength={50}
                                          placeholder='프로젝트 설명은 최대 50자 입력이 가능합니다.' autoComplete='off' onChange={onProjectDescChange}  />
                            </dd>
                        </dl>
                    </div>
                </div>
                <div className='lyr_btm'>
                    <div className='btnBox sz_small'>
                        <button className='btn_lyr_close' onClick={close}>취소</button>
                        <button id='btn_pjt_add' className='btn_lyr_close' type='button' disabled={btnDisable} onClick={callApiCreate}>등록하기
                        </button>

                    </div>
                </div>
            </div>
        </div>
    );
});

// @ts-ignore
export default React.memo(PopCreateProject);
