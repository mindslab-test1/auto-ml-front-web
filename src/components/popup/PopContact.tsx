import React, {useContext, useEffect, useState} from 'react';
import ContactItem from './ContactItem';
import RootStore from 'store/RootStore';
import {API, callApi} from '../../service/ApiService';
import ComInput from '../commons/ComInput';

interface Props {
  close:  () => void;
  popTitle: string;
  modelId: boolean;
  modelName: string;
}

const PopContact: React.FC<Props> = ({close, popTitle,modelId,modelName}) => {

  const {userStore, notiStore} = useContext(RootStore);
  const {sttProjectStore} = useContext(RootStore);
  const [showLabel, setShowLabel] = useState(true);
  const [btnActive, setBtnActive] = useState(true);
  const [userText, setUserText] = useState('');
  const [userPhone, setUserPhone] = useState('');

  const handleTyping = (e) => {
    if(e.target.value.length !== 0){
      setShowLabel(false);
    }else{
      setShowLabel(true);
    }
    setUserText(e.target.value);
  }
  const onPhoneTxt= (e) => {
    setUserPhone(e.target.value)
  }
  useEffect(() => {
    (userText.length && userPhone.length > 0) ? setBtnActive(false): setBtnActive(true)
  },[userText,userPhone]);

  const sendMail = async() => {
    if ( userText.length > 0 ) {
      try{
        let param;
        if(modelId){
          param = {
            name : userStore.name,
            email : userStore.email,
            modelName : modelName,
            inquiryDesc : userText,
            phoneNumber : userPhone,
          };
        }else {
          param = {
            name : userStore.name,
            email : userStore.email,
            inquiryDesc : userText,
            phoneNumber : userPhone,
          };
        }
        const res = await callApi(API.INQUIRY_REG, param);
        console.dir('Result : ', res.data);
        notiStore.infoBlue('문의가 접수되었습니다. 담당자 확인 후 연락드리겠습니다.');
      }catch (err){
        console.error(err);
        notiStore.error('[실패] 잠시 후 다시 시도해 주세요.');
      }
      close();
    }else{
      notiStore.error('문의 내용을 입력해 주세요.');
    }
  }

  return (
    <div className='lyrWrap'>
      <div className='lyr_bg' onClick={close}/>
      <div className='lyrBox contactBox'>
        <div className='contact_tit'>
          <h3>{popTitle}</h3>
          <p className='info_txt'>모델에 관한 문의를 남겨주시면 담당자가 확인 후, 답변 메일을 보내드리겠습니다.</p>
        </div>
        <div className='contact_cnt'>
          <ul className='contact_lst'>
            <li><a href='tel:1661-3222'>1661-3222</a></li>
            <li><span>hello@mindslab.ai</span></li>
          </ul>
          <div className='contact_form'>
            <ContactItem iconClass={'fas fa-user'} span={'이름'} value={userStore.name}/>
            <ContactItem iconClass={'fas fa-envelope'} span={'이메일'} value={userStore.email}/>
            <div className='contact_item'>
              <dl className='dlBox'>
                <dt><span className='fas fa-mobile-alt' />연락처</dt>
                <ComInput ddClass={'enable'} type={'number'} className={'ipt_txt'} value={userPhone} onChange={onPhoneTxt} afterText={''} defaultTxt={'- 없이 입력해 주세요.'} disabled={false}/>
              </dl>
            </div>
            {modelId && <div className='contact_item_block'><dl className='dlBox'>
                <dt><span className='fas fa-cube'/>모델아이디</dt><dd className='ft_clr'>{modelName}</dd></dl>
            </div>}
            <div className='contact_item_block'>
              <textarea value={userText} className='textArea' onChange={handleTyping} />
              <label htmlFor='textArea' style={{display: showLabel ? '': 'none'}}><span className='fas fa-align-left' />문의내용</label>
              <p className='msg'>※ 문의내용에 욕설, 성희롱 등의 내용이 포함된 경우 문의가 제한될 수 있습니다.</p>
            </div>
          </div>
        </div>
        <div className='contact_btn'>
          <button className='btn_inquiry' onClick={sendMail} disabled={btnActive}>문의하기</button>
          <button className='btn_lyr_close' onClick={close} >닫기</button>
        </div>
      </div>
    </div>
  );
};

export default PopContact;
