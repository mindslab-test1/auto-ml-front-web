import React from 'react';


interface Props {
    close:  () => void;
}

const PopModelApi: React.FC<Props> = ({close}) => {

        return (
            <div className='lyrWrap'>
                <div className='lyr_bg' onClick={close}/>
                {/*API조회 */}
                <div id='lyr_apiInfo' className='lyrBox'>
                    <div className='lyr_top'>
                        <h3>모델 API</h3>
                        <button className='btn_lyr_close' onClick={close}>닫기</button>
                    </div>
                    <div className='lyr_mid'>
                        <div className='dlBox'>
                            <dl className='dl_tbl'>
                                <dt>모델 아이디</dt>
                                <dd>userId_voice_20200916130221_STT_kor_16k</dd>
                                {/*// <!-- [D] 모델 아이디: 사용자ID_학습데이터Type_생성일시분_엔진type_샘플링레이트 -->*/}
                            </dl>
                            <dl className='dl_tbl txtByteCheck'>
                                <dt>모델 API 명</dt>
                                <dd className='ft_clr'>dkdk </dd>
                                <dd className='bg_g'>url -X POST \
                                    https://api.maum.ai/api/stt/ \
                                    -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
                                    -F ID= (사용자 ID값 출력 (CloudAPI - API정보에서 확인가능)) \
                                    -F key= (사용자 key 출력 (CloudAPI - API정보에서 확인가능)) \
                                    -F lang= (①데이터선택에서 설정한 언어 ( kor / eng )값 출력) \
                                    -F sampling= (①데이터선택에서 설정한 sampling rate (8000 / 16000)값 출력) \
                                    -F model= (④학습결과확인에서 선택한 학습모델 출력,CNN STT방식) \
                                    -F 'file= (④테스트에서 (녹음:웹상에서 녹음한 파일, 업로드:사용자가 업로드한 파일) 출력)'
                                </dd>
                            </dl>
                        </div>
                    </div>
                    <div className='lyr_btm'>
                        <div className='btnBox sz_small'>
                            <button className='btn_lyr_close' onClick={close}>닫기</button>
                        </div>
                    </div>
                </div>
                {/*API조회 */}
            </div>
        );
};

export default PopModelApi;
