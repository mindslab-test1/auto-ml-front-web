import React, {useContext, useState} from 'react';
import {API, callApi} from '../../service/ApiService';
import RootStore from '../../store/RootStore';
import {deepCopyObject} from '../../utils/CommonUtils';
import HistoryTableRow from '../commons/HistoryTableRow';


interface Props {

    close:  () => void;
    historyList: any;
}

const PopHistoryList: React.FC<Props> = (({close,historyList}) => {

    const {sttProjectStore, notiStore} = useContext(RootStore);
    const [selectedRowId, setSelectedRowId] = useState(-1)

    const callProjectAsSnapshot = async () => {

        const _api = deepCopyObject(API.PROJECT_SNAPSHOT_APPLY);
        _api.url = _api.url.replace('_ID_',sttProjectStore.id).replace('_SNAPSHOT_ID_', selectedRowId);

        await callApi(_api).then((res)=>{
            if(res.status === 200){
                console.dir(res.data);
                notiStore.infoGreen('snapshot 불러오기 성공');
                sttProjectStore.fetchData(sttProjectStore.id);
                close();
            }else{
                notiStore.error('snapshot 불러오기 실패');
                console.log('%c HTTP status : ' + res.status + ' : change project data fail\n', res.data);
            }
        }).catch((res)=>{
            notiStore.error('snapshot 불러오기 실패');
            console.log('%c HTTP status : ' + res.status + ' : change project data fail\n', res.data);
        })
    }

    return (
        <div className='lyrWrap' >
            <div className='lyr_bg' onClick={close}/>
            {/*학습이력조회*/}
            <div id='lyr_learnHisSrch' className='lyrBox'>
                <div className='lyr_top'>
                    <h3>학습이력조회</h3>
                    <button className='btn_lyr_close' onClick={close}>닫기</button>
                </div>
                <div className='lyr_mid'>
                    <div className='tbl_lstBox'>
                        <table id='tbl_learnHisSrch'>
                            <caption className='hide'>학습이력목록</caption>
                            <colgroup>
                                <col /><col width='200' /><col width='200' /><col width='200' /><col width='150' />
                            </colgroup>
                            <thead>
                            <tr>
                                <th scope='col'>학습모델 명</th>
                                <th scope='col'>선택 된 데이터</th>
                                <th scope='col'>학습설정</th>
                                <th scope='col'>학습모니터링 결과</th>
                                <th scope='col'>등록일</th>
                            </tr>
                            </thead>
                            <tbody>
                            {historyList.map((item)=>((
                                <HistoryTableRow key={item.id} item={item} onClick={setSelectedRowId} selectedRowId={selectedRowId}/>
                            )))}
                            </tbody>
                        </table>
                    </div>
                </div>
                <div className='lyr_btm'>
                    <div className='btnBox sz_small'>
                        <button className='btn_lyr_close' onClick={close}>닫기</button>
                        <button id='btn_learnImport' className='btn_lyr_close' disabled={selectedRowId === -1} onClick={callProjectAsSnapshot}>불러오기</button>
                    </div>
                </div>
            </div>
            {/*학습이력조회 */}
        </div>
    );
});

export default React.memo(PopHistoryList);
