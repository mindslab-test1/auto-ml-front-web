import React, {useContext, useState} from 'react';
import TextCounter from '../commons/ComTextCounter';
import {API, callApi} from '../../service/ApiService';
import RootStore from '../../store/RootStore';
import {deepCopyObject} from '../../utils/CommonUtils';


interface Props {
    stepId: number;
    close:  () => void;
    setPopShow: Function;
}

const PopSnapShot: React.FC<Props> = ({stepId, close, setPopShow}) => {

    const {sttProjectStore, trainingStore, notiStore} = useContext(RootStore);

    const [modelName, setModelName] = useState('');
    const [btnDisable, setBtnDisable] = useState(true);


    const callRegSnapShot = async () => {

        console.log('%c popModelName : ', 'color:green', modelName);

        let param = {name : modelName};
        const _api = deepCopyObject(API.PROJECT_SNAPSHOT_REG);
        _api.url = _api.url.replace('_ID_', sttProjectStore.id);

        await callApi(_api, param).then((res)=>{
            if(res.status === 200){
                console.dir(res.data);
                trainingStore.setSnapshotId(res.data.id);
                trainingStore.setModelId(res.data.modelId);
                callBeginTraining();
                notiStore.infoGreen('학습 모델이 등록되었습니다.');
                setPopShow(false);
            }else{
                notiStore.error('학습 모델 등록 실패');
                console.log('%c HTTP status : ' + res.status + ' : regSnapshot fail\n', res.data);
            }
        }).catch((res)=>{
            notiStore.error('학습 모델 등록 실패');
            console.log('%c HTTP status : ' + res.status + ' : regSnapshot fail\n', res.data);
        });
    }


    const callBeginTraining = async () => {

        let param = {id : trainingStore.getSnapshotId(), modelId: trainingStore.getModelId()};
        const _api = deepCopyObject(API.TRAINING_BEGIN);

        await callApi(_api, param).then((res)=>{
            if(res.status === 200){
                console.dir(res.data);

                sttProjectStore.setStep(stepId+1);
                notiStore.infoGreen('학습 시작 요청 성공');
            }else{
                notiStore.error('학습 시작 요청 실패');
                console.log('%c HTTP status : ' + res.status + ' : regSnapshot fail\n', res.data);
            }
        }).catch((res)=>{
            notiStore.error('학습 시작 요청 실패');
            console.log('%c HTTP status : ' + res.status + ' : regSnapshot fail\n', res.data);
        })

    }


    const handleTextCount = (e) => {
        setModelName(e.target.value);
        (e.target.value.length === 0) ? setBtnDisable(true) : setBtnDisable(false);
    }

    return (
        <div className='lyrWrap' >
            <div className='lyr_bg' onClick={close}/>
            {/*학습모델등록*/}
            <div id='lyr_modelName_add' className='lyrBox'>
                <div className='lyr_top'>
                    <h3>학습모델 등록</h3>
                    <button className='btn_lyr_close' onClick={close}>닫기</button>
                </div>
                <div className='lyr_mid'>
                    <div className='dlBox'>
                        <dl className='dl_tbl'>
                            {/*<dt>학습모델 아이디</dt>
                            <dd>userId_voice_20200916130221_STT_kor_16k</dd>*/}
                            {/*[D] STT학습모델 아이디 규칙: 사용자ID_학습데이터Type_생성일시분_엔진type_샘플링레이트*/}
                        </dl>
                        <dl className='dl_tbl txtByteCheck'>
                            <dt>학습모델 명 <span className='byteBox'><TextCounter length={modelName.length} maxLength={30}/> </span></dt>
                            <dd>
                                <input type='text' id='model_name' className='ipt_txt w100p txtByte' maxLength={30} placeholder='모델 명은 최대 30자 입력이 가능합니다.' autoComplete='off' onChange={handleTextCount} />
                            </dd>
                        </dl>
                    </div>
                </div>
                <div className='lyr_btm'>
                    <div className='btnBox sz_small'>
                        <button id='btn_model_add' onClick={callRegSnapShot} disabled={btnDisable} >학습 시작</button>
                    </div>
                </div>
            </div>
            {/*//학습모델등록*/}
        </div>
    );
};

export default PopSnapShot;
