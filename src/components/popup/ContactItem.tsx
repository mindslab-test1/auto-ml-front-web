import React, {useContext} from 'react';
import RootStore from '../../store/RootStore';


interface Props{
    iconClass : string,
    span: string,
    value: string,
}

const ContactItem: React.FC<Props> = ({iconClass,span, value}) => {
    const {userStore} = useContext(RootStore);
    return (
        <div className='contact_item'>
            <dl className='dlBox'>
                <dt><span className={iconClass}/>{span}</dt>
                <dd>{value}</dd>
            </dl>
        </div>
    )
}

export default ContactItem;
