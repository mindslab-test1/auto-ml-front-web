import React, {useContext, useState} from 'react';
import ComPopBox from './ComPopBox';
import PopContact from '../popup/PopContact';
import imgVisual01 from 'assets/images/img_visual01.jpg';
import RootStore from '../../store/RootStore';
import {observer} from 'mobx-react-lite';

interface Props {

}

const ModelContact: React.FC<Props> = () => {

  const { userStore } = useContext(RootStore);
  const [popShow, setPopShow] = useState(false);

  const setPopIsOpen = () => {
     console.log('Model == setPopIsOpen');
     setPopShow(!popShow);
  }

  const closePopup = () => {
      console.log(popShow);
      setPopShow(false);
  }

  return(

      <div className='svc_sta'>

          <div className='svc_visual'>
              <dl>
                  <dt>autoML</dt>
                  <dd>AI 모델학습</dd>
              </dl>
              <p className='bg_img'><img src={imgVisual01} alt='autoML 이미지' /></p>
          </div>

          <div className='lnb'>
              <h2>autoML</h2>
              {/* [D] 로컬메뉴*/}
              {/*<ul class='nav'>*/}
              {/*    <li><a class='active' href='#none'>로컬메뉴 1</a></li>*/}
              {/*    <li><a href='#none'>로컬메뉴2</a></li>*/}
              {/*    <li><a href='#none'>로컬메뉴3</a></li>*/}
              {/*</ul>*/}

              <ul className='nav_etc'>
                {(userStore.email === '' || userStore.email === undefined) ?
                  '' :
                  <li><a id='btn_contact' className='btn_rou_pstive btn_lyr_open' onClick={setPopIsOpen}>다른 모델 문의하기</a></li>}

              </ul>
          </div>
          <ComPopBox show={popShow}>
              <PopContact close={closePopup} popTitle={'다른 모델 문의하기'} modelId={false} modelName={''}/>
          </ComPopBox>
      </div>

 );

};

export default observer(ModelContact);
