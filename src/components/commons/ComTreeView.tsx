import React from 'react';

interface Props {
  data: any[];
  selectedGroup: string;
  selectedMember: string;
   onClick: (fileName: any, mdlId: any, sampleRate) => void;
}

const ComTreeView: React.FC<Props> = ({data, onClick, selectedGroup, selectedMember}) => {
  return (
    <>
      {
        (data && data.length > 0) ?
          data.map((item, index) => {
            return (
              <li key={index}><span className='folder'>{item.key}</span>
                <ul>
                  {item.value.map((it, index) => {
                    // console.log(it);
                    const style = (it === selectedMember && item.key === selectedGroup) ? 'file active' : 'file';
                    return (
                      <li key={index} onClick={() => onClick(it, item.key, item.sampleRate)}><span className={style}>{it}</span></li>
                    )
                  })}
                </ul>
              </li>
            )
          })
        :
          <li><span className='file'>모델 정보가 없습니다.</span></li>
      }
    </>
  );
};

export default ComTreeView;
