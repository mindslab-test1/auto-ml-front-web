import React from 'react';

interface Props {
    dataItem:any;
}

const HistoryUseDataRow: React.FC<Props> = ({dataItem}) => {
  return(
      <tr className='show'>
          <td scope='row'>{dataItem.atchFileDisp}</td>
          <td>{dataItem.smplRate === 'SR001' ? '8K' : '16K'}</td>
          <td>{dataItem.playSec}</td>
          <td>{dataItem.playSecAvg}</td>
          <td>{dataItem.useEosYn}</td>
      </tr>
  )
}

export default React.memo(HistoryUseDataRow);
