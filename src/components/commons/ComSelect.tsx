import React from 'react';

interface Props {
  onChange: (e) => void;
  options: any;
  keyName: string;
  dispName: string;
  value: any;
}

const ComSelect: React.FC<Props> = ({onChange, options, keyName, dispName, value}) => {
  return (
    <dd>
      <select className='select w100p' value={value} onChange={onChange}>
        {options.map(
          (option, index) => <option key={index} value={option[keyName]}>{option[dispName]}</option>
        )}
      </select>
    </dd>
  );
};

export default ComSelect;
