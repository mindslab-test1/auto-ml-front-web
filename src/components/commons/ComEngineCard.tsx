import React, {useRef, useState} from 'react';

interface Props {
    setPopIsOpen: (engineName: string, engineLowerCase: string ) => void;
    imgPath: any;
    engineName: string;
    engineLowerCase: string;
    title: string;
    text: string;
}

const ComEngineCard: React.FC<Props> = ({setPopIsOpen, imgPath, engineName,engineLowerCase,title,text}) => {
    const [cls, setCls] = useState('btn_lyr_open');

    const handleMouseEnter = () => {
        setCls('btn_lyr_open hover');
    }
    const handleMouseLeave = () => {
        setCls('btn_lyr_open');
    }

    return(
        <li className={engineLowerCase}>
            <a className={cls} href={void(0)} onClick={()=>setPopIsOpen(engineName, engineLowerCase)} onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>
                <span className='category'>{engineName}</span>
                <span className='thumb'>
                    <img src={imgPath} alt={engineName} />
                </span>
                <span className='tit'>{title}</span>
                <span className='txt'>{text}</span>
            </a>
        </li>
    );
}

export default ComEngineCard;
