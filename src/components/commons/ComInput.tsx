import React from 'react';
import {observer} from 'mobx-react-lite';

interface Props {
    ddClass : string;
    type : string;
    className : string;
    value : any;
    afterText : string;
    onChange : (e) => void;
    defaultTxt: string;
    disabled: boolean;
}

const ComInput: React.FC<Props> = ({ddClass,type, className, value, afterText, onChange, defaultTxt,disabled}) => {
    return(
        <dd className={ddClass}>
            <input type={type} className={className} value={value} onChange={onChange} placeholder={defaultTxt} disabled={disabled}/>{afterText}
        </dd>
    );
}

export default observer(ComInput);
