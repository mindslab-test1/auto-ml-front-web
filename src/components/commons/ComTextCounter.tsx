import React from 'react';

interface Props {
  length: number;
  maxLength: number;
}

const ComTextCounter: React.FC<Props> = React.memo(({length,maxLength}) => {
    return(
        <>
            <em className='count' id='projectNameCount'>{(length<maxLength)? length : maxLength}</em>/
            <em className='maxcount'>{maxLength}</em>
        </>
    )
});

export default React.memo(ComTextCounter);
