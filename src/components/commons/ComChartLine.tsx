import React, {useState} from 'react';
import Chart from 'react-apexcharts';

interface Props {
  categories : number[];
  graphData : any;
}

const ComChartLine : React.FC<Props> = ({categories, graphData}) => {

  const [options, setOptions] = useState({
    chart: {
      id: 'basic-bar',
      zoom: {
        enabled: false,
      }
    },
    dataLabels: {
      enabled: false
    },
    xaxis: {
      categories:categories
    }

  });

  let series = [];
  graphData.forEach((value, key) => {
    let data = new Set();
    data['name'] = key;
    data['data'] = value;

    // @ts-ignore
    series.push(data);
  });

  return(
    <div id='chart1' >
      <Chart
        options={options}
        series={series}
        type='line'
        height='200'
        width='240px'
      />
    </div>
  );
}

export default ComChartLine;
