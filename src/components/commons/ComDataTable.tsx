import React from 'react';
import {observer} from 'mobx-react-lite';

interface Props {
  onClick: (id) => void;
  columns: any;
  data: any;
  title: string;
}

const ComDataTable: React.FC<Props> = ({title, columns, data, onClick}) => {
  // console.log('in ComDataTable');
  // console.log(data);
  return (
    <dd className='tbl_lstBox scroll_s'>
      <table id='srch_dataLst' className='tbl_multiSelect'>
        <caption className='hide'>{title}</caption>
        <thead>
          <tr>
            {columns.map(
              (column, index) => <th scope='col' key={index}>{column.title}</th>
            )}
          </tr>
        </thead>
        <tbody>
        {
          (data && data.length > 0) ?
            data.map(
              (item, index) => (
                <tr onClick={() => onClick(item.atchFileId)} key={index}>
                  {columns.map(({dataIndex, displayFtn}, index) => <td key={index}> {displayFtn(item[dataIndex])} </td> )}
                </tr>
              )
            )
          :
            <tr id='srchDataNone'>
              <td scope='row' colSpan={5} className='dataNone'>사용 가능한 데이터가 없습니다.</td>
            </tr>
        }
        </tbody>
      </table>
    </dd>
  );
};

export default observer(ComDataTable);
