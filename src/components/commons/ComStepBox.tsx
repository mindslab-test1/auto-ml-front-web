import React, {Dispatch, SetStateAction, useContext, useEffect, useRef, useState} from 'react';
import RootStore from '../../store/RootStore';
import {observer} from 'mobx-react-lite';


interface Props {
    id: number;
    stepNum: string;
    stepTitle: string;
    stepDesc: string;
}

const ComStepBox : React.FC<Props> = ({id, stepNum, stepTitle, stepDesc, children}) =>{

  const {sttProjectStore} = useContext(RootStore);
  const {getStep, setStep, getCompletedStep, setCompletedStep} = sttProjectStore;

  const [cellBoxClass, setCellBoxClass] = useState('cellBox ');
  const stepBoxHelpIcon = useRef<HTMLDListElement>(null);

  // id: my id
  // getStep(): previous id
  // getCompletedStep(): completed step id

  const updateClass = () => {
    if(id <= getCompletedStep()) {
      if (id !== getStep()) {
        if(getCompletedStep() < 2) {
          setCellBoxClass('cellBox sum');
        }
        else {
          setCellBoxClass('cellBox sum disabled');
        }
      }
      else {
        setCellBoxClass ('cellBox');
      }
    }else{
      setCellBoxClass('cellBox disb');
    }
  }

  const updateActive = () => {
    if (id !== getStep()) {
      if (id <= getCompletedStep()) {
        if(getCompletedStep() < 2) {
          setStep(id);
        }
      }
    }
  }

  const handleMouseEnter = () => {
    if(stepBoxHelpIcon.current) stepBoxHelpIcon.current.classList.add('show');
  }
  const handleMouseLeave = () => {
    if(stepBoxHelpIcon.current) stepBoxHelpIcon.current.classList.remove('show');
  }


  useEffect( () => {
    // console.log('IN ComStepBox useEffect:' + id + ', ' + getStep());
    updateClass();
  })

  return (
    <div className={(id === sttProjectStore.currentStep) ? 'lot_cell active' : 'lot_cell'} onClick={updateActive}> {/* [D] Box활성화 될 경우만 .active 선언 */}
      <div className={cellBoxClass} id={'step'+stepNum}>
        <dl className='step_helpBox' ref={stepBoxHelpIcon}>
          <dt><span>STEP {stepNum}.</span>{stepTitle}</dt>
          <dd>{stepDesc}</dd>
        </dl>
        {/* .cell_top */}
        <div className='cell_top'>
          <h4><span>STEP {stepNum}.</span>{stepTitle}</h4>
          <span className='btn_step_help' onMouseEnter={handleMouseEnter} onMouseLeave={handleMouseLeave}>?</span>
        </div>
        {/* //.cell_top */}
        {children}
      </div>
    </div>
  );
}

export default observer(ComStepBox);
