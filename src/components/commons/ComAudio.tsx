import React from 'react';

interface Props {
  src: any;
}

const ComAudio : React.FC<Props> = ({src}) => {
  console.log('ComAudio: src:' + src);
  return (
    <div style={{opacity:'0',height:'0px'}}>
      <audio src={src} controls autoPlay playsInline style={{height:'0px'}}/>
    </div>
  );
};

export default ComAudio;
