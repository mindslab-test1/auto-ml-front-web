import React from 'react';

interface Props {
    totalPages : number;
    paginate :Function;
    currentPage :number;
}

const Paging: React.FC<Props> = ({totalPages,paginate,currentPage}) => {

    const pageNumbers:number[] = [];

    for(let i = 1; i <= totalPages; i++){pageNumbers.push(i);}

    const goFirst = () => {paginate(1)};

    const goPrev = () => {if(currentPage !== 1){paginate(currentPage-1)}};

    const goNext = () => {if(currentPage !== totalPages){paginate(currentPage+1)}};

    const goEnd = () => {paginate(totalPages)};

    return (
        <div className='pageing'>
            <a className='first' href={void(0)} onClick={goFirst}>맨 처음 페이지로</a>
            <a className='prev' href={void(0)} onClick={goPrev}>이전 페이지로</a>
            {pageNumbers.map(
                (pagingNumber,index) =>
                    currentPage === index+1 ? <strong key={index}>{pagingNumber}</strong> : <a key={index} href={void(0)} onClick={() => paginate(pagingNumber)}>{pagingNumber}</a>
            )}
            <a className='next' href={void(0)} onClick={goNext}>다음 페이지로</a>
            <a className='end' href={void(0)} onClick={goEnd}>맨 마지막 페이지로</a>
        </div>
    )
}
export default React.memo(Paging);