import React from 'react';

interface Props {
    noiseItem:any;
}

const HistoryNoiseRow: React.FC<Props> = ({noiseItem}) => {
  return(
      <tr className='show'>
          <td scope='row'>{noiseItem.atchFileDisp}</td>
          <td>{noiseItem.smplRate === 'SR001' ? '8K' : '16K'}</td>
          <td>{noiseItem.playSec}</td>
          <td>{noiseItem.playSecAvg}</td>
      </tr>
  )
}

export default React.memo(HistoryNoiseRow);
