import React, {useEffect} from 'react';
import Parser from 'html-react-parser';

interface Props {
  title: string;
  desc: string;
}

const ComHelpIcon: React.FC<Props> = ({title, desc}) => {

  useEffect(()=>{
    // console.log('ComHelpIcon useEffect');
  })

  return (
    <dt>{title}
      <span className='ico_help'><em>{Parser(desc)}</em></span>
    </dt>
  );
};

export default ComHelpIcon;
