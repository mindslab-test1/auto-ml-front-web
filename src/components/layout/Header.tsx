import React, {useContext, useState} from 'react';
import { observer } from 'mobx-react-lite';

import RootStore from '../../store/RootStore';
import ComLoginBox from './header/ComLoginBox';
import ComLanguageBox from './header/ComLanguageBox';
import ComAppBox from './header/ComAppBox';
import ModelContact from '../commons/ModelContact';


interface Props {}

const Header: React.FC<Props> = () => {


  const {layoutStore} = useContext(RootStore);
  const { showHeader } = layoutStore;


  if (showHeader) {
    return (

          <div id='header'>
            <div className='maum_sta'>
              <h1><a href='https://maum.ai'>maum.ai</a></h1>

              <div className='maum_gnb'>
                <ul className='nav'>
                  <li><a href='https://maum.ai/?lang=ko#service_position'>서비스</a></li>
                  <li><a href='https://maum.ai/home/academyForm'>마음 아카데미</a></li>
                  <li><a href=''>AI 컨설턴트 모집</a></li>
                </ul>
              </div>

              <div className='maum_etc'>
                <ul className='nav'>
                  <li><a href='https://maum.ai/home/pricingPage'>가격정책</a></li>
                  <ComLoginBox />
                  <ComLanguageBox />
                  <ComAppBox />
                </ul>
              </div>

            </div>

           <ModelContact />

          </div>

    );
  } else {
    return <div />;
  }
};

export default observer(Header);
