import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';

interface Props {
    columns: any;
    data: any;
    onClick: (id) => void;
    stepId:number;
}

const Step2NoiseData: React.FC<Props> = ({columns, data, onClick,stepId}) => {

    const {sttProjectStore} = useContext(RootStore);

    let tblWidth;
    if(stepId !== sttProjectStore.currentStep){
        tblWidth = 90 + (60 * sttProjectStore.noiseDataList.length)+'px';
    }else {
        tblWidth = 100+'%';
    }

    return (
        <dd className='tbl_lstBox scroll_s'>
        <table id='tbl_noiseData' className='tbl_multiSelect' style={{width:tblWidth}}>
            <caption className='hide'>Noise Data Set 목록</caption>
            <colgroup>
                <col /><col /><col /><col /><col />
            </colgroup>
            <thead>
                <tr>
                    {columns.map((column, index) =>
                        <th scope='col' key={index}>{column.title}</th>
                    )}
                </tr>
            </thead>
            <tbody>
            {
                (data && data.length > 0) ?
                    data.map((item, index) => {
                        if( sttProjectStore.noiseDataList.every((x) => { return (x.atchFileId !== item.atchFileId)}) ){
                            return <tr key={index} onClick={() => onClick(item.atchFileId)} className=''>
                                        {columns.map(({dataIndex, displayFtn}, index) => <td key={index}> {displayFtn(item[dataIndex])} </td>)}
                                   </tr>
                        }
                        else {
                            return <tr key={index} onClick={() => onClick(item.atchFileId)} className='active'>
                                        {columns.map(({dataIndex, displayFtn}, index) => <td key={index}> {displayFtn(item[dataIndex])} </td>)}
                                   </tr>
                        }
                    })
                :
                    <tr>
                        <td colSpan={5} className='dataNone'>준비 된 데이터가 없습니다.</td>
                    </tr>
            }
            </tbody>
        </table>
    </dd>
    );
};

export default observer(Step2NoiseData);
