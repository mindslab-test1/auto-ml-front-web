import React, {useContext, useState} from 'react';

import RootStore from '../../../store/RootStore';
import ComHelpIcon from '../../commons/ComHelpIcon';
import ComSelect from '../../commons/ComSelect';
import Step1AvailableLearningDataTable from './Step1AvailableLearningDataTable';
import Step1SelectedLearningDataTable from './Step1SelectedLearningDataTable';
import Step1NextBtn from './Step1NextBtn';
import {observer} from 'mobx-react-lite';

interface Props {
  id : number;
}

const Step1 : React.FC<Props> = ({id}) => {
  // console.log('%c In Step1', 'color:red');
  const {sttProjectStore, legacyDataStore, notiStore} = useContext(RootStore);
  const [params, setParams] = useState(['']);

  const saveLearningData = e => {
    // e.stopPropagation();
    // validate inputs

    if(sttProjectStore.learningDataList.length < 1) {
      notiStore.warning('학습 데이터를 1개 이상 선택해 주세요.');
      return false;
    }

    void sttProjectStore.saveLearningData(id); // learningData 저장 및 성공시 setStep
  }

  const EOS_DATA = [
    // {key: null, value : '전체'},
    {key: 'Y', value : 'Y'},
    {key: 'N', value : 'N'},
  ];

  const onLanguageChange = (e) => {
    sttProjectStore.setLanguage(e.target.value);
    onSearch();
  }

  const onSampleRateChange = (e) => {
    sttProjectStore.setSampleRate(e.target.value);
    sttProjectStore.setBatchSize(1);
    onSearch();
  }

  const onEosChange = (e) => {
    sttProjectStore.setEos(e.target.value);
    onSearch();
  }

  const onSearch = () => {
    void legacyDataStore.fetchLearningData(true);
    notiStore.infoBlue('Learning data loaded.');
  }

  return (
    <>
      {/* .cell_mid */}
      <div className='cell_mid'>
        <div className='lot_item'>
          <dl>
            <ComHelpIcon title='언어' desc='조회할 데이터 언어 형태를 선택합니다.'/>
            <ComSelect value={sttProjectStore.language} onChange={onLanguageChange} options={legacyDataStore.getLang()} keyName={'code'} dispName={'codeDisp'}/>
          </dl>
          <dl>
            <ComHelpIcon title='샘플링레이트' desc='텍스트 변환에 사용한 오디오 주파수를 선택합니다.'/>
            <ComSelect value={sttProjectStore.sampleRate} onChange={onSampleRateChange} options={legacyDataStore.getSampleRate()} keyName={'code'} dispName={'codeDisp'}/>
          </dl>
          <dl>
            <ComHelpIcon title='Use EOS' desc='데이터에서 문장분리의 여부를 선택합니다.'/>
            <ComSelect value={sttProjectStore.eos} onChange={onEosChange} options={EOS_DATA} keyName={'key'} dispName={'value'}/>
          </dl>
        </div>
        <div className='btnSqBox'>
          {/*<button type='button' onClick={onSearch}>조회</button>*/}
        </div>

        <div className='lot_item sum_hide'>
          <dl>
            <ComHelpIcon title='사용 가능한 데이터' desc='maumDATA에 등록한 데이터 목록입니다.'/>
            <Step1AvailableLearningDataTable/>
          </dl>
        </div>
        <div className='lot_item'>
          <dl>
            <ComHelpIcon title='학습에 사용할 데이터' desc='학습에 사용할 데이터 입니다.'/>
            <Step1SelectedLearningDataTable stepId={id}/>
          </dl>
        </div>
      </div>
      {/* //.cell_mid */}

      {/* .cell_btm */}
      <Step1NextBtn title={'저장 후 이동'} onClick={saveLearningData}/>
      {/* //.cell_btm */}
    </>
  );
}

export default observer(Step1);
