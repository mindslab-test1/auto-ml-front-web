import React, { useState} from 'react';

import Step2DetailSetting from './Step2DetailSetting';

interface Props{}

const Step2ToggleBox: React.FC<Props> = () => {

    const [activeDiv, setActiveDiv] = useState(false);

    const onClickDetailSetting = () => {
        setActiveDiv(!activeDiv);
    }

    return(
        <div className={activeDiv ? 'toggleBox active' : 'toggleBox'}>
            <Step2DetailSetting handleClick={onClickDetailSetting}/>
        </div>
    );
}

export default Step2ToggleBox;
