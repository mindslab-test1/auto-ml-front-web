import React, {useContext, useEffect} from 'react';
import RootStore from '../../../store/RootStore';
import ComHelpIcon from '../../commons/ComHelpIcon';
import ComInput from '../../commons/ComInput';
import {observer} from 'mobx-react-lite';

interface Props {
    handleClick: () => void;
}

const Step2DetailSetting: React.FC<Props> = ({handleClick}) => {

    const {sttProjectStore} = useContext(RootStore);

    const onMinNoiseSampleChange = (e) => {
        sttProjectStore.setMinNoiseSample(parseInt(e.target.value, 10));
    }

    const onMaxNoiseSampleChange = (e) => {
        sttProjectStore.setMaxNoiseSample(parseInt(e.target.value, 10));
    }

    const onMinSnrChange = (e) => {
        sttProjectStore.setMinSnr(parseInt(e.target.value, 10));
    }

    const onMaxSnrChange = (e) => {
        sttProjectStore.setMaxSnr(parseInt(e.target.value, 10));
    }

    useEffect(()=>{
        sttProjectStore.minSnr = 5;
        sttProjectStore.maxSnr = 20;

        if(sttProjectStore.sampleRate === 'SR001'){
            sttProjectStore.minNoiseSample = 200;
            sttProjectStore.maxNoiseSample = 4800000
        }else {
            sttProjectStore.minNoiseSample = 400;
            sttProjectStore.maxNoiseSample = 9600000
        }

    },[sttProjectStore.sampleRate])

    return(
        <>
            <h5>
                <button type='button' onClick={handleClick}>상세설정</button>
            </h5>
            <div className='toggle'>
                <div className='lot_item '>
                    <dl>
                        <ComHelpIcon title='Min noise sample' desc='노이즈를 추출할 최소 길이를 설정합니다.<br />*길이:샘플 단위'/>
                        <ComInput ddClass={''} type={'number'} className={'ipt_txt w100p'}
                                  value={sttProjectStore.minNoiseSample}
                                /*value={sttProjectStore.getMinNoiseSample() !== 0 ?
                                      sttProjectStore.getMinNoiseSample()
                                      : (sttProjectStore.getSampleRate() === legacyDataStore.sampleRate[0].code ? 200 : 400)
                                  }*/
                                  onChange={onMinNoiseSampleChange}
                                  afterText={''}
                                  defaultTxt={''}
                                  disabled={true}
                        />
                        {/* [D] 기본값 200(8k), 400(16k), Min값은 max값 보다 클수 없음 */}
                    </dl>
                    <dl>
                        <ComHelpIcon title='Max noise sample' desc='노이즈를 추출할 최대 길이를 설정합니다.<br />*길이:샘플 단위'/>
                        <ComInput ddClass={''} type={'number'} className={'ipt_txt w100p'} value={sttProjectStore.maxNoiseSample} onChange={onMaxNoiseSampleChange} afterText={''} defaultTxt={''} disabled={true}/>
                        {/* [D] 기본값(김준영 차장님에게 문의), max값은 Min값 보다 작을수 없음 */}
                    </dl>
                </div>
                <div className='lot_item '>
                    <dl>
                        <ComHelpIcon title='Min SNR' desc='값이 작을수록 노이즈가 크게 더해집니다.'/>
                        <ComInput ddClass={''} type={'number'} className={'ipt_txt w100p'} value={sttProjectStore.minSnr} onChange={onMinSnrChange} afterText={''} defaultTxt={''} disabled={false}/>
                        {/* [D] 기본값 5.0(범위 조면철 전무님에게 문의), Min값은 max값 보다 클수 없음 */}
                    </dl>
                    <dl>
                        <ComHelpIcon title='Max SNR' desc='값이 클수록 노이즈가 작게 더해집니다.'/>
                        <ComInput ddClass={''} type={'number'} className={'ipt_txt w100p'} value={sttProjectStore.maxSnr} onChange={onMaxSnrChange} afterText={''} defaultTxt={'' } disabled={false}/>
                        {/* [D] 기본값 20.0(범위 조면철 전무님에게 문의), max값은 Min값 보다 작을수 없음 */}
                    </dl>
                </div>
            </div>
        </>
    );
}

export default observer(Step2DetailSetting);
