import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import ComNextBtn from '../../commons/ComNextBtn';

interface Props {
    title: string;
    onClick: (e) => void;
}

const Step3NextBtn: React.FC<Props> = ({title, onClick}) => {

    const {sttProjectStore} = useContext(RootStore);
    const btnState = false;/*!(sttProjectStore.learningDataList && sttProjectStore.learningDataList.length !== 0);*/

    return ( <ComNextBtn title={title} onClick={onClick} disabled={btnState}/> );
};

export default observer(Step3NextBtn);
