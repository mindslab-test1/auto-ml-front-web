import React, {useContext} from 'react';
import SelectedLearningData from './Step1SelectedLearningData';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import {defaultDisplay, smplRateDisplay} from '../../../utils/CommonUtils';

interface Props {
  stepId:number;
}

const LIST_COLUMNS = [
  { title: 'Name', dataIndex: 'atchFileDisp', displayFtn: defaultDisplay },
  { title: '샘플링레이트', dataIndex: 'smplRate', displayFtn: smplRateDisplay },
  { title: '전체음성길이', dataIndex: 'playSec', displayFtn: defaultDisplay },
  { title: '평균음성길이', dataIndex: 'playSecAvg', displayFtn: defaultDisplay },
  { title: 'EOS 여부', dataIndex: 'useEosYn', displayFtn: defaultDisplay },
];

const Step1SelectedLearningDataTable: React.FC<Props> = ({stepId}) => {
  // console.log('in ComSelectedLearningDataTable');
  const {sttProjectStore, legacyDataStore} = useContext(RootStore);

  const onClick = (id) => {
    console.log('ComSelectedLearningDataTable onClick:' + id);
    const item = sttProjectStore.learningDataList.find(it => it.atchFileId === id);
    sttProjectStore.takeLearningData(item);
  }

  return (
      <SelectedLearningData title={'학습에 사용할 데이터 목록'} columns={LIST_COLUMNS} data={sttProjectStore.learningDataList} onClick={onClick} stepId={stepId}/>
  );
};

export default observer(Step1SelectedLearningDataTable);
