import React, {useContext, useEffect, useState} from 'react';
import ComPopBox from '../../commons/ComPopBox';
import PopHistoryList from '../../popup/PopHistoryList';
import {deepCopyObject} from '../../../utils/CommonUtils';
import {API, callApi} from '../../../service/ApiService';
import RootStore from '../../../store/RootStore';
import Step4ModelTreeView from './Step4ModelTreeView';
import {observer} from 'mobx-react-lite';
import Step4ModelTestContainer from './Step4ModelTestContainer';
import Step4NextBtn from './Step4NextBtn';

interface Props {
    id : number;
}

const Step4 : React.FC<Props> = ({id}) => {
    const {sttProjectStore, sttModelTestStore} = useContext(RootStore);
    const [historyList, setHistoryList] = useState([]);
    const [popShow, setPopShow] = useState(false);
    const [stepNum, setStepNum] = useState('1')
    const [disb, setDisb] = useState(true);
    const getSnapShot = async(id) => {
        try{
            const _api = deepCopyObject(API.PROJECT_SNAPSHOT_LIST);
            _api.url = _api.url.replace('_ID_', id);
            const res = await callApi(_api);
            console.dir(res.data);
            setHistoryList(res.data);
            setPopShow(!popShow);

        }catch (err){
            console.error(err);
        }
    }

    const closePopup = () => {
        setPopShow(false);
    }
    sttModelTestStore.setStepNum(1);
    let fileActive = document.getElementsByClassName('file');
    useEffect( () => {
        for(let i=0; i <fileActive.length; i ++){
            fileActive[i].classList.remove('active')
        }
    },[]);


    return (
        <>
            {/* .cell_mid */}
            <div className='cell_mid'>
                <div className='lot_item'>
                    <dl>
                        <dt>학습모델
                            <span className='ico_help'><em>학습이 완료된 모델과 데이터입니다.</em></span>
                            <a id='btn_learnHisSrch' className='btn_lyr_open' onClick={()=>getSnapShot(sttProjectStore.id)}>학습이력조회</a>
                        </dt>
                        <Step4ModelTreeView disb={setDisb}/>
                    </dl>
                </div>
                {/*lot_item */}
                <div className='lot_item'>
                    <dl>
                        <dt>심플 테스트
                            <span className='ico_help'><em>학습이 완료된 데이터를 선택하여 간단한 테스트를 진행할 수 있습니다.</em></span>
                        </dt>
                        <dd className='enTestBox'>
                            <Step4ModelTestContainer disb={disb}/>
                        </dd>
                    </dl>
                </div>
            </div>
            {/* //.cell_mid */}

            {/* .cell_btm */}
            <Step4NextBtn title={'문의하기'} />
            {/*<div className='cell_btm'>
                <div className='btnSqBox'>
                    <a id='btn_apiAdd' className='btn_lyr_open disabled'>API 생성하기</a>
                </div>
            </div>*/}
            {/* //.cell_btm */}

            <ComPopBox show={popShow}>
                <PopHistoryList close={closePopup} historyList={historyList} />
            </ComPopBox>
        </>
    );
}

export default observer(Step4);
