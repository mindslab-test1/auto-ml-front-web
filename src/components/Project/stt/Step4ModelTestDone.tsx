import React, {useContext, useEffect, useState} from 'react';
import RootStore from '../../../store/RootStore';

interface Props {

}

const Step4ModelTestDone : React.FC<Props> = () => {
    const {sttModelTestStore} = useContext(RootStore);
    const [result, setResult] = useState('');

    const recordBtn = () => {
        sttModelTestStore.setStepNum(1);
    }

    useEffect(()=>{
        setResult(sttModelTestStore.getModelTestResult());
    },[])
    return (
        <>
            {/*step05(결과)*/}
            <div className='testStep step05'>
                <div className='setpBox'>
                    <div className='resultBox scroll_s'>
                        {result}
                    </div>
                </div>
                <div className='btnBox'>
                    <button type='button' className='ico_reset btn_clr' onClick={recordBtn}>처음으로</button>
                </div>
            </div>
            {/*//step05*/}
        </>
    );
}

export default Step4ModelTestDone;
