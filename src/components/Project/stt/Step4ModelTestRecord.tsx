import React, {useContext, useEffect, useState} from 'react';
import RootStore from '../../../store/RootStore';
import {captureUserMedia} from '../../../utils/CommonUtils';
import RecordRTC from 'recordrtc';
import {observer} from 'mobx-react-lite';

interface Props {
}

const hasGetUserMedia = !!(navigator.getUserMedia);

const Step4ModelTestRecord : React.FC<Props> = () => {
    const {sttModelTestStore, notiStore} = useContext(RootStore);

    const [src, setSrc] = useState('');
    const [blob, setBlob] = useState('');
    const [sampleRate , setsampleRate] = useState(8000);
    let recorder = null;

    useEffect( () => {
        console.log('Step4ModelTestRecord useEffect');
        if(!hasGetUserMedia) {
            alert('Your browser cannot stream from your mic. Please switch to Chrome or Firefox.');
            return;
        }
        if(src === '') {requestUserMedia();}
        startRecord();
    })

    const requestUserMedia = () => {
        console.log('requestUserMedia')
        captureUserMedia((mediaStream) => {
            setSrc(mediaStream);
        });
    }

    const startRecord = () => {
        console.log('startRecord');
        const sampleRateValue = sttModelTestStore.currentModel.smplRate;
        sampleRateValue === 'SR001' ? setsampleRate(8000) : setsampleRate(16000)

        captureUserMedia((mediaStream) => {
            console.log('record started');
            recorder = RecordRTC(mediaStream,{
                type: 'audio',
                recorderType: RecordRTC.StereoAudioRecorder,
                numberOfAudioChannels: 1,
                desiredSampRate: sampleRate, // 샘플레이트 값 넣기
            });
            // @ts-ignore
            recorder.startRecording();
            // @ts-ignore
            recorder.microphone = mediaStream;

        });
    }

    const stopRecord = () => {
        console.log('stopRecord');
        let mdId = sttModelTestStore.currentModel.mdlId;
        let mdlFileName = sttModelTestStore.currentModel.mdlFileName;
        let mdlFilePath = sttModelTestStore.currentModel.mdlFilePath;
        // @ts-ignore
        recorder.stopRecording(() => {
            // @ts-ignore
            const blob = recorder.getBlob()
            setBlob(blob);
            setSrc(URL.createObjectURL(blob));
            // @ts-ignore
            recorder.microphone.stop();
            console.log('record stopped');
            sttModelTestStore.fetchSttTest(blob, mdId, mdlFileName, mdlFilePath).then(r =>console.log('녹음 성공') );
        });

    }

    return (
        <>
            {/*Step02(녹음 화면)*/}
            <div className='testStep step02'>
                <div className='setpBox'>
                    <div className='ani_voice'>
                        <span className='voiceBar01' />
                        <span className='voiceBar02' />
                        <span className='voiceBar03' />
                        <span className='voiceBar04' />
                        <span className='voiceBar05' />
                    </div>
                    <p className='ani_mesg'>음성을 듣고 있습니다.<br />완료버튼을 클릭하시면 바로 변환을 시작합니다.</p>
                    <div className='btnBox'>
                        {/*<ComAudio src={src}/>*/}
                        <button type='button' className='ico_stop' onClick={stopRecord}>완료</button>
                    </div>
                </div>
            </div>
            {/*//Step02*/}
        </>
    );
}

export default observer(Step4ModelTestRecord);
