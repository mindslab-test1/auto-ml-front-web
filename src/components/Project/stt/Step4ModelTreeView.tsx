import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import ComTreeView from '../../commons/ComTreeView';
import {observer} from 'mobx-react-lite';

interface Props {
  disb:any;
}

const Step4ModelTreeView : React.FC<Props> = ({disb}) => {
  const {sttModelTestStore} = useContext(RootStore);
  const testModel = sttModelTestStore.getTestModelList;
  const selectedFileName = (sttModelTestStore.currentModel && sttModelTestStore.currentModel.mdlFileName)
    ? sttModelTestStore.currentModel.mdlFileName : '';
  const selectedModelName = (sttModelTestStore.currentModel && sttModelTestStore.currentModel.mdlFileName)
    ? sttModelTestStore.currentModel.mdlId : '';

  const onClick = (name, id, sampleRate) => {
    // console.log(name);
    sttModelTestStore.setCurrentModel(name, id);
    disb(false);
  }

  return (
    <dd className='treeBox scroll_s' id='treeSelect'>
      <ul className='filetree'>
        <ComTreeView data={testModel} selectedMember={selectedFileName} selectedGroup={selectedModelName} onClick={onClick} />
      </ul>
    </dd>
  );
};

export default observer(Step4ModelTreeView);
