import React, {useContext, useEffect, useState} from 'react';
import RootStore from '../../../store/RootStore';

interface Props {
    disb:boolean;
}

const Step4UploadBtn : React.FC<Props> = ({disb}) => {
    const {sttModelTestStore } = useContext(RootStore);
    let style = disb ? 'ico_upload disabled' : 'ico_upload';

    const onChange = () => {
        // sttModelTestStore.setStepNum(3);
        // @ts-ignore
        let file = document.getElementById('ipt_file').files[0];
        const mdId = sttModelTestStore.currentModel.mdlId;
        const mdlFileName = sttModelTestStore.currentModel.mdlFileName;
        const mdlFilePath = sttModelTestStore.currentModel.mdlFilePath;
        sttModelTestStore.fetchSttTest(file, mdId, mdlFileName, mdlFilePath).then(r =>console.log('업로드 성공') );

    }

    return (
        <>
            <div className='fileBox'>
                <label htmlFor='ipt_file' className={style}>파일 업로드</label>
                <input type='file' id='ipt_file' accept='.wav' onChange={onChange}/>
            </div>

        </>
    );
}

export default Step4UploadBtn;