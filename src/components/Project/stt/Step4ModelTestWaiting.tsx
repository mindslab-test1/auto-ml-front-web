import React, {useContext, useEffect, useState} from 'react';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';

interface Props {

}

const Step4ModelTestWaiting : React.FC<Props> = () => {
    const {sttModelTestStore} = useContext(RootStore);
    const [queueIndex, setQueueIndex] = useState(0);
    let interval;
    const recordBtn = () => {
        console.log('recordBtn')
        clearInterval(interval);
        const id = sttModelTestStore.getModelTestSnapshotId();
        console.log(id);
        sttModelTestStore.fetchSttTestCancel(id).then(r => console.log('테스트 취소'));
    }
    console.log(queueIndex);
    useEffect(()=>{
        setQueueIndex(sttModelTestStore.queueIndex);
        interval = setInterval( async () => {
            sttModelTestStore.fetchSttTestStatus().then(r => {
                console.log('Waiting: 상태 조회');
                console.log(sttModelTestStore.getQueueIndex());
                setQueueIndex(sttModelTestStore.queueIndex);
            })
        },10000);
        return () => {
            clearInterval(interval);
        }
    },[]);

    return (
        <>
            {/*step03(대기열 화면)*/}
            <div className='testStep step03'>
                <div className='setpBox'>
                    <div className='ani_timer' />
                    <p className='ani_mesg'>잠시만 기다려 주세요. <br />현재 <span className='ft_point'>{queueIndex}</span>개의 테스트가 대기 중 입니다.</p>
                    <div className='btnBox'>
                        <button type='button' className='ico_stop' onClick={recordBtn}>취소</button>
                    </div>
                </div>
            </div>
            {/*step03*/}
        </>
    );
}

export default observer(Step4ModelTestWaiting);
