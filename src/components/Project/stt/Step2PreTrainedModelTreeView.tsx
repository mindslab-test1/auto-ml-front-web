import React, {useContext} from 'react';
import RootStore from '../../../store/RootStore';
import ComTreeView from '../../commons/ComTreeView';
import {observer} from 'mobx-react-lite';

interface Props {
}

const Step2PreTrainedModelTreeView : React.FC<Props> = () => {
  const {sttProjectStore, legacyDataStore} = useContext(RootStore);
  const {preTrainedModelCommonListDisplay, preTrainedModelPrivateListDisplay} = legacyDataStore;

  const selectedFileName = (sttProjectStore.preTrainedModel && sttProjectStore.preTrainedModel.mdlFileName)
    ? sttProjectStore.preTrainedModel.mdlFileName : '';

  const selectedModelName = (sttProjectStore.preTrainedModel && sttProjectStore.preTrainedModel.mdlFileName)
    ? sttProjectStore.preTrainedModel.mdlId : '';

  const onClickCommon = (name, id) => {
    // console.log('onClickCommon:' + name);
    sttProjectStore.setPreTrainedModel(name, id, true);
  }

  const onClickPrivate = (name, id) => {
    // console.log('onClickPrivate:' + name);
    sttProjectStore.setPreTrainedModel(name, id, false);
  }

  return (
    <dd className='treeBox scroll_s' id='treeSelect'>
      <ul className='filetree'>
        <li><span className='folder'>공통 모델</span>
          <ul>
            <ComTreeView data={preTrainedModelCommonListDisplay} selectedMember={selectedFileName}
                         selectedGroup={selectedModelName} onClick={onClickCommon} />
          </ul>
        </li>
        <li><span className='folder'>내가 만든 모델</span>
          <ul>
            <ComTreeView data={preTrainedModelPrivateListDisplay} selectedMember={selectedFileName}
                         selectedGroup={selectedModelName} onClick={onClickPrivate} />
          </ul>
        </li>
      </ul>
    </dd>
  );
};

export default observer(Step2PreTrainedModelTreeView);
