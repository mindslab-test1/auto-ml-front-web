import React, {useContext} from 'react';
import ComDataTable from '../../commons/ComDataTable';
import RootStore from '../../../store/RootStore';
import {observer} from 'mobx-react-lite';
import {defaultDisplay, smplRateDisplay} from '../../../utils/CommonUtils';

interface Props {
  // onClick: (e) => void;
}

const LIST_COLUMNS = [
  { title: 'Name', dataIndex: 'atchFileDisp', displayFtn: defaultDisplay },
  { title: '샘플링레이트', dataIndex: 'smplRate', displayFtn: smplRateDisplay },
  { title: '전체음성길이', dataIndex: 'playSec', displayFtn: defaultDisplay },
  { title: '평균음성길이', dataIndex: 'playSecAvg', displayFtn: defaultDisplay },
  { title: 'EOS 여부', dataIndex: 'useEosYn', displayFtn: defaultDisplay },
];

const Step1AvailableLearningDataTable: React.FC<Props> = () => {
  // console.log('in ComAvailableLearningDataTable');
  const {sttProjectStore, legacyDataStore} = useContext(RootStore);

  const onClick = (id) => {
    console.log('ComAvailableLearningDataTable onClick:' + id);
    const item = legacyDataStore.learningDataList.find(it => it.atchFileId === id);
    sttProjectStore.putLearningData(item);
  }

  return (
      <ComDataTable title={'사용 가능한 데이터 목록'} columns={LIST_COLUMNS} data={legacyDataStore.getAvailLearnData} onClick={onClick}/>
  );
};

export default observer(Step1AvailableLearningDataTable);
