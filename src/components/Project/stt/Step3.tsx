import React, {useContext, useEffect, useState} from 'react';
import RootStore from '../../../store/RootStore';
import Step3NextBtn from './Step3NextBtn';
import Step3Waiting from './Step3Waiting';
import Step3Training from './Step3Training';
import Step3Error from './Step3Error';
import {observer} from 'mobx-react-lite';
import {deepCopyObject} from '../../../utils/CommonUtils';
import {API, callApi} from '../../../service/ApiService';

interface Props {
    id : number;
}

const Step3 : React.FC<Props> = ({id}) => {
    const {sttProjectStore, trainingStore, notiStore} = useContext(RootStore);
    const [btnDisable, setBtnDisable] = useState(true); // next 버튼 활성화 여부

    const saveTrainingEnd = (e) => {
        e.stopPropagation();
        void finishTraining();
    }

    const finishTraining = async () => {
        console.log(trainingStore.getSnapshotId(),  trainingStore.getModelId());

        let param = {id : trainingStore.getSnapshotId(), modelId: trainingStore.getModelId()};
        const _api = deepCopyObject(API.TRAINING_FINISH);

        await callApi(_api, param).then((res)=>{
            if(res.status === 200){
                console.dir(res.data);
                if(res.data.success === 'fail'){
                    console.log('%c'+res.data.message + '\nerrorCode:' + res.data.errorCode, 'color:red');
                    notiStore.error('학습 종료 실패');
                }else{
                    sttProjectStore.setStep(sttProjectStore.getStep()+1);
                    notiStore.infoGreen('학습 종료 성공');
                }

            }else{
                notiStore.error('학습 종료 실패');
                console.log('%c HTTP status : ' + res.status + ' : finish training fail\n', res.data);
            }
        }).catch((res)=>{
            notiStore.error('학습 종료 실패');
            console.log('%c HTTP status : ' + res.status + ' : finish training fail\n', res.data);
        })
    }


    let midComponent;
    switch (trainingStore.status){
        case 'q' :
            midComponent = <Step3Waiting />
            trainingStore.fetchTrainingDataUpdate(sttProjectStore.id).then(r => console.log('training status: q, update: 데이터 상태 조회'));
            break;
        case 't' :
            midComponent = <Step3Training />
            trainingStore.fetchTrainingDataUpdate(sttProjectStore.id).then(r => console.log('training status: t, update: 데이터 상태 조회'));
            break;
        case 'c' :
            midComponent = <Step3Training />
            if(sttProjectStore.completedStep === 2){
                void sttProjectStore.updateStep(sttProjectStore.id, sttProjectStore.completedStep + 1)
            }
            // trainingStore.fetchTrainingDataUpdate(sttProjectStore.id).then(r => console.log('update: 데이터 상태 조회'));
            break;
        case 'e' :
            midComponent = <Step3Error />
            break;
        default :
            midComponent = <div />
            break;
    }
    // useEffect(()=>{
    //     trainingStore.fetchTrainingDataUpdate(sttProjectStore.id).then(r => console.log('update: 데이터 상태 조회'));
    // },[])
    return (
        <>
            {/* .cell_mid */}
            <div className='cell_mid'>

                {midComponent}


                {/*선행 학습 있을 때 (선행학습 없을 시, 해당 div remove 해주세요.) */}
                {/*<div className='fifo'>
                    <div className='aniBox'><img src='../../../../public/image/ani_fifo.gif' alt='학습 대기중'/></div>
                    <p className='tit'>잠시만 기다려 주세요.<br />현재 <span className='ft_point_orange'>000</span>개의 <span className='ft_point_orange'>학습이 대기 중</span>입니다.</p>
                    <p className='txt'>안정적인 학습 환경을 제공하기 위해서<br />학습을 제한하고 있습니다.</p>
                    <p className='txt_s'>학습 취소 버튼을 클릭 하실 경우, <br />대기 순위가 초기화 될 수 있습니다.</p>
                    <button type='button' className='btn_learning_cancel'>학습 취소</button>
                </div>*/}

                {/* //선행 학습 있을 때 */}

                {/* 선행 학습 없을 때 */}
                {/*<div className='fifo_none'>
                    <div className='lot_item'>
                        <dl>
                            <dt>학습모델 아이디
                                <span className='ico_help'><em>학습모델의 고유코드입니다.</em></span>
                            </dt>
                            <dd>userId_voice_20200916130221_STT_kor_16k</dd>
                             [D] STT학습모델 아이디 규칙: 사용자ID_학습데이터Type_생성일시분_엔진type_샘플링레이트
                        </dl>
                    </div>
                    <div className='lot_item'>
                        <dl>
                            <dt>Loss
                                <span className='ico_help'><em>학습 모니터링 오류 비율입니다.</em></span>
                            </dt>
                            <dd className='chartBox'><Chart2 /></dd>
                        </dl>
                    </div>
                    <div className='lot_item'>
                        <dl>
                            <dt>Token Error Rate
                                <span className='ico_help'><em>학습 모니터링에서 토큰 단위로 오류가 있는 비율입니다.<br />*토큰(Token)이란 문법적으로 더 이상 나눌 수 없는 언어 요소</em></span>
                            </dt>
                            <dd className='chartBox'><ComChartLine /></dd>
                        </dl>
                        <dl>
                            <dt>Word Error Rate
                                <span className='ico_help'><em>학습 모니터링에서 단어 단위로 오류가 있는 비율입니다.</em></span>
                            </dt>
                            <dd className='chartBox'><ComChartLine /></dd>
                        </dl>
                    </div>
                </div>*/}
                {/* //선행 학습 없을 때 */}

                {/* 학습에러 시 */}
                {/*<div className='fifo_error'>
                    <div className='aniBox'><img src='/public/image/ani_error.gif' alt='학습 에러'/></div>
                    <p className='tit'>죄송합니다. <br/>지금 서비스와 연결할 수 없습니다.</p>
                    <p className='txt'>현재 문제를 해결하기 위해 담당부서에서 확인 중에 있습니다.<br/>서비스 이용에 불편을 드려 대단히 죄송합니다.</p>
                    <p className='txt_s'></p>
                    <button type='button' className='btn_learning_cancel'>이전 단계로 돌아가기</button>
                </div>*/}
                {/* //학습에러 시 */}

            </div>
            {/* //.cell_mid */}

            {/* .cell_btm */}
            {/*<div className='cell_btm'>*/}
            {/*    <div className='btnSqBox'>*/}
            {/*        <a href='#lyr_modelName_add' id='btn_learning_end' className='btn_lyr_open'>학습종료</a>*/}
            {/*    </div>*/}
            {/*</div>*/}
            <Step3NextBtn title={'학습종료'} onClick={saveTrainingEnd} />
            {/* //.cell_btm */}
        </>
    );
}

export default observer(Step3);
