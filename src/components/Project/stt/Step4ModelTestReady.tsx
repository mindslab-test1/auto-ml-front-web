import React, {useEffect} from 'react';
import Step4RecordBtn from './Step4RecordBtn';
import Step4UploadBtn from './Step4UploadBtn';
import {observer} from 'mobx-react-lite';

interface Props {
    disb:boolean;
}

const Step4ModelTestReady : React.FC<Props> = ({disb}) => {


    useEffect(()=>{

    },[]);

    return (
        <>
            {/*Step01(첫 화면)*/}
            <div className='testStep step01'>
                <div className='setpBox'>
                    {!disb?
                        <p className='infoTxt'>녹음 버튼을 클릭하고 녹음을 시작하거나, <br />음성파일을 업로드해 주세요.</p>
                        :  <p className='infoTxt'>학습 모델에서 테스트 할 데이터를 선택해주세요.</p>
                    }
                        <div className='btnBox'>
                        <Step4RecordBtn disb={disb}/>
                        <Step4UploadBtn disb={disb}/>
                    </div>
                </div>
                <ul className='lst_info'>
                    <li>*지원가능한 파일 확장자 : .wav</li>
                    <li>*5MB 이하의 음성 파일을 이용해 주세요.</li>
                </ul>
            </div>
            {/*//Step01*/}
        </>
    );
}

export default observer(Step4ModelTestReady);
