import _PageProjectCreate from './PageProjectCreate';
import _PageProjectList from './PageProjectList';
import _PageModelList from './PageModelList';
import _PageProjectDetail from './PageProjectDetail';

export const PageProjectCreate = _PageProjectCreate;
export const PageProjectList = _PageProjectList;
export const PageModelList = _PageModelList;
export const PageProjectDetail = _PageProjectDetail;
