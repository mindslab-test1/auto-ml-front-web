import _PageSsoLogin from './PageSsoLogin'
import _PageLoginComplete from './PageLoginComplete'

export const PageSsoLogin = _PageSsoLogin;
export const PageLoginComplete = _PageLoginComplete;
