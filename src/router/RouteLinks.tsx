import {PageLoginComplete, PageSsoLogin} from 'pages/auth';
import {PageProjectCreate, PageProjectList, PageModelList, PageProjectDetail} from '../pages/project';
import {PageError, PageNetworkError} from '../pages/error';

export enum ERouteUrl {
  HOME = '/',
  SSO_LOGIN = '/sso_login',
  LOGIN_COMPLETE = '/login/complete',
  PROJECT_CREATE = '/prj/new',
  PROJECT_LIST = '/prj/list',
  MODEL_API = '/prj/modelApi',
  PROJECT_DETAIL = '/prj/:id',
  ERROR_GENERAL = '/error',
  ERROR_NETWORK = '/network_error',

}

export const ROUTES = [
  { path: ERouteUrl.HOME, exact: true, component: PageProjectCreate },
  { path: ERouteUrl.SSO_LOGIN, exact: true, component: PageSsoLogin },
  { path: ERouteUrl.LOGIN_COMPLETE, exact: true, component: PageLoginComplete },
  { path: ERouteUrl.PROJECT_CREATE, exact: true, component: PageProjectCreate },
  { path: ERouteUrl.PROJECT_LIST, exact: true, component: PageProjectList },
  { path: ERouteUrl.MODEL_API, exact: true, component: PageModelList },
  { path: ERouteUrl.PROJECT_DETAIL, exact: true, component: PageProjectDetail },
  { path: ERouteUrl.ERROR_GENERAL, exact: true, component: PageError },
  { path: ERouteUrl.ERROR_NETWORK, exact: true, component: PageNetworkError },

];
